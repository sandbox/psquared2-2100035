(function($){
	$.fn.wysiwygEvt = function(){
		return this.each(function(){
			var $this = $(this);
			var htmlold = $this.html();
			$this.bind('blur keyup paste copy cut', function(){ //http://stackoverflow.com/questions/6256342/trigger-an-event-when-contenteditable-is-changed
				var htmlnew = $this.html();
				if (htmlold !== htmlnew) {
					$this.trigger('change');
				}
			})
		})
	}
	
	$.fn.highlight = function() { //http://stackoverflow.com/questions/848797/yellow-fade-effect-with-jquery
		$(this).each(function() {
			var el = $(this);
			el.before("<div/>")
			el.prev()
			.width(el.width())
			.height(el.height())
			.css({
				"position": "absolute",
				"background-color": "#00FF00",
				"opacity": ".1"   
			})
			.fadeOut(500);
		});
	}	
	
	var grandeSaveList = [];
	
	Drupal.behaviors.grande = {
	  	attach:function(context, settings){
		  	grande.bind(context.getElementsByClassName('.grandejs'));
		  	$('article.grandejs[contenteditable=true]', context).wysiwygEvt().change(function(event){
			  	var element = $(this);
			  	var id = element.attr('id');
			  	
			  	if($.inArray(id, grandeSaveList) === -1){ //since inArray returning 0 means index 0 *sigh*
			  		grandeSaveList.push(id);
			  	}
		  	});
		}
	};
	
	var id = setInterval(function(){
		for(i in grandeSaveList){
			var element = $('article#' + grandeSaveList[i]).first();
			var nid = element.attr('data-nid'), field = element.attr('data-field'), delta = element.attr('data-delta');
		
			$.ajax({
				url:Drupal.settings.basePath + 'grandejs/' + nid + '/' + field + '/' + delta,
				type:'post',
				data:{
					'data':element.html()
				}
			});
			element.highlight();
			
			grandeSaveList = []; //empty out the array to start over*/
		}
	}, 10000);
	
})(jQuery);