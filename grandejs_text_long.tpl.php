<?php
	drupal_add_js(drupal_get_path('module', 'grandejs').'/grandejs/grande.js', array('scope'=>'footer'));
	drupal_add_js(drupal_get_path('module', 'grandejs').'/grandejs_adapter.js', array('scope'=>'footer'));	
	drupal_add_css(drupal_get_path('module', 'grandejs').'/grandejs_adapter.css', array('group'=>'CSS_THEME'));	
	drupal_add_css(drupal_get_path('module', 'grandejs').'/grandejs/editor.css', array('group'=>'CSS_THEME'));
	drupal_add_css(drupal_get_path('module', 'grandejs').'/grandejs/menu.css', array('group'=>'CSS_THEME'));
?>

<article class="grandejs" data-nid="<?php print($nid); ?>" data-field="<?php print($field_name); ?>" data-delta="<?php print($delta); ?>" id="grandejs_content_<?php print($nid.'_'.$field_name.'_'.$delta); ?>" contenteditable="true">
	<div><?php print($content); ?></div>
</article>